export const projects = [
  {
    title: 'Baristalk',
    description: "Réseau social pour les amateurs de café.",
      image: '/images/1.png',
      tags: ['Laravel', 'Tailwind', 'MySQL', 'Javascript'],
    source: 'https://google.com',
    visit: 'https://gitlab.com/Giyom-webdev/baristalk.v2',
    id: 0,
  },
  {
    title: 'faitmonmenage.com',
    description:"Création d’un site web avec un CMS pour gérer le système de gestion d’embauche de femmes de ménage.",
    image: '/images/2.png',
    tags: ['Javascript', 'Node', 'Express', 'HTML', 'CSS'],
    visit: 'https://gitlab.com/Dmorel571/tp4-production-api',
    id: 1,
  },
  {
    title: 'Orange Marketing',
    description: "un site pour la vente de support au niveau Marketing dans le monde du web",
      image: '/images/3.png',
      tags: ['REST API', 'MongoDB', 'NodeJS', 'Javascript'],
    visit: 'https://gitlab.com/Dmorel571/tp4-integrateur',
    id: 2,
  },
];

export const TimeLineData = [
  { year: 2017, text: 'Started my journey', },
  { year: 2018, text: 'Worked as a freelance developer', },
  { year: 2019, text: 'Founded JavaScript Mastery', },
  { year: 2020, text: 'Shared my projects with the world', },
  { year: 2021, text: 'Started my own platform', },
];