import Link from 'next/link';
import React from 'react';
import { AiFillGitlab, AiFillLinkedin } from 'react-icons/ai';

import { Container, Div1, Div2, Div3, NavLink, SocialIcons, Span } from './HeaderStyles';

const Header = () =>  (
    <Container>
    <Div2>
      <li>
        <Link href="#projects">
          <NavLink>Projets</NavLink>
        </Link>
      </li>
      <li>
        <Link href="#tech">
          <NavLink>Technologies</NavLink>
        </Link>
      </li>        
      <li>
        <Link href="#about">
          <NavLink>À propos</NavLink>
        </Link>
      </li>        
    </Div2>
    <Div3>
        <SocialIcons href="https://gitlab.com/Dmorel571">
          <AiFillGitlab size="3rem" />
        </SocialIcons>
        <SocialIcons href="https://www.linkedin.com/in/david-morel-606573111/">
          <AiFillLinkedin size="3rem" />
        </SocialIcons>
      </Div3>
  </Container>
);

export default Header;
