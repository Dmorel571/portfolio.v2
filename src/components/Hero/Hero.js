import React from 'react';

import { Section, SectionText, SectionTitle } from '../../styles/GlobalComponents';
import Button from '../../styles/GlobalComponents/Button';
import { LeftSection } from './HeroStyles';

const Hero = (props) => (
  <Section row nopadding>
    <LeftSection>
      <SectionTitle main center>
      Mon nom est <br/>
      David Morel
      </SectionTitle>
      <SectionText>
        Je suis un étudiant en développement web à la recherche d'un stage afin d'approfondir mes connaissances !
      </SectionText>
      <Button onClick={() => window.location = "mailto: dmorel571@gmail.com"}>Envoyer un courriel</Button>
    </LeftSection>
  </Section>
);

export default Hero;