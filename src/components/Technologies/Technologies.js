import React from 'react';
import { DiCss3, DiDatabase, DiFirebase, DiNodejsSmall, DiReact } from 'react-icons/di';
import { FiFigma } from 'react-icons/fi';
import { Section, SectionDivider, SectionText, SectionTitle } from '../../styles/GlobalComponents';
import { List, ListContainer, ListItem, ListParagraph, ListTitle } from './TechnologiesStyles';

const Technologies = () =>  (
  <Section id="tech">
      <SectionTitle>Technologies</SectionTitle>
      <SectionText>
        J'ai travaillé avec plusieurs technologies dans le monde du développement web lors de ma formation. Du Back-end au Design
      </SectionText>
      <List>
        <ListItem>
          <DiReact size="3rem"/>
          <ListContainer>
            <ListTitle>Front-End</ListTitle>
            <ListParagraph>
              Expérience avec <br/>
              React.JS
            </ListParagraph>
          </ListContainer>
        </ListItem>
        <ListItem>
          <DiFirebase size="3rem"/>
          <ListContainer>
            <ListTitle>Back-End</ListTitle>
            <ListParagraph>
              Expérience avec <br/>
              Node.JS <br/> 
              base de données MySQL
            </ListParagraph>
          </ListContainer>
        </ListItem>
        <ListItem>
          <FiFigma/>
          <ListContainer>
            <ListTitle>Design</ListTitle>
            <ListParagraph>
              Maquettes avec <br/>
              Figma
            </ListParagraph>
          </ListContainer>
        </ListItem>
      </List>
  </Section>
);

export default Technologies;
