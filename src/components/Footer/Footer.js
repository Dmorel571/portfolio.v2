import React from 'react';
import { AiFillGitlab, AiFillLinkedin } from 'react-icons/ai';

import { SocialIcons } from '../Header/HeaderStyles';
import { FooterTextContainer, FooterWrapper, LinkColumn, LinkItem, LinkList, LinkTitle, Slogan, SocialContainer, SocialIconsContainer } from './FooterStyles';

const Footer = () => {
  return (
    <FooterWrapper>
      <LinkList>
      <LinkColumn>
      <LinkTitle>Email</LinkTitle>
      <LinkItem href= "mailto: dmorel571@gmail.com">dmorel571@gmail.com</LinkItem>
      </LinkColumn>
      </LinkList>
      <SocialIconsContainer>
        <FooterTextContainer>
          <Slogan>N'oubliez pas d'aller voir mes autres projets</Slogan>
        </FooterTextContainer>
        <SocialContainer>
      <SocialIcons href="https://gitlab.com/Dmorel571">
          <AiFillGitlab size="3rem" />
        </SocialIcons>
        <SocialIcons href="https://www.linkedin.com/in/david-morel-606573111/">
          <AiFillLinkedin size="3rem" />
        </SocialIcons>
        </SocialContainer>
      </SocialIconsContainer>
    </FooterWrapper>
  );
};

export default Footer;
