
import { Section, SectionDivider, SectionText, SectionTitle } from '../../styles/GlobalComponents';


const Timeline = () => {

  return (
    <Section id="about">

      <SectionTitle>À propos de moi</SectionTitle>
      <SectionText>Chaque jour il y a quelque chose de nouveau à découvrir sur le web et je n’arrête pas d’en vouloir plus malgré tout ! Que ce soit l’architecture, le marketing, un nouveau framework, je veux tout connaître.</SectionText>
    </Section>
  );
};

export default Timeline;
